import "./App.css";
import Popup from "./components/Popup";
import Header from "./components/Header";
import { Routes, Route, useLocation } from "react-router-dom";
import SlideContainer from "./components/SlideContainer";
import Fulfilment from "./components/Benefit/Fulfilment";
import Solutions from "./components/Solutions/Solutions";
import Introduce from "./components/Introduce/Introduce";
import Consulting from "./components/Consulting/Consulting";
import MetaTag from "./components/MetaTag";
import PolicyPrivacy from "./components/Policy/PolicyPrivacy";

function App() {
  const location = useLocation();
  const pathname = location.pathname;
  return (
    <div className="App">
      <MetaTag />
      {pathname !== "/consulting" && pathname !== "/policy" && (
        <>
          <Popup />
        </>
      )}
      {pathname !== "/policy" && <Header />}
      <Routes>
        <Route path="/" element={<SlideContainer />} />
        <Route path="/benefit/*" element={<Fulfilment />} />
        <Route path="/solution/*" element={<Solutions />} />
        <Route path="/introduce/*" element={<Introduce />} />
        <Route path="/consulting/*" element={<Consulting />} />
        <Route path="/policy/*" element={<PolicyPrivacy />} />
      </Routes>
    </div>
  );
}

export default App;
