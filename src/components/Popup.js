import React, { useState } from "react";
import "../assets/css/Popup.css";
import Container from "../assets/images/Popup/Container.png";
import Popup_Logo from "../assets/images/Popup/Popup_Logo.png";
import Close_Btn from "../assets/images/Popup/Close_Btn.png";
import { Link } from "react-router-dom";
import PopupImg from "../assets/images/Popup/new_popup.png";

function Popup() {
  const [isPopupOpen, setIsPopupOpen] = useState(true);

  const closePopup = () => {
    setIsPopupOpen(false);
  };

  return (
    <div style={{ display: isPopupOpen ? "block" : "none", position: "relative" }}>
      <div id="PopupOverlay"></div>
      <div id="PopupContent">
        <Link href="#" className="cheat_close" onClick={closePopup} />
        <img src={PopupImg} className="main_img" alt="popup" />
        <Link to="/consulting" className="cheat_link" />
        {/* <a href="/consulting" className="cheat_link"></a> */}
      </div>
    </div>
  );
}

export default Popup;
