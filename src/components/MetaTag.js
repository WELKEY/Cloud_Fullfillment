import React from "react";
import { Helmet } from "react-helmet-async";

const MetaTag = ({ title, description, url, image }) => {
  const metaData = {
    title: title ?? "클라우드 풀필먼트",
    description: description ?? "물류비절감과 매출전략을 한번에! 클라우드풀필먼트",
    url: url ?? "https://www.happotech.net",
    image: image ?? "https://happotech.net/MetaLogo.png",
  };
  return (
    <Helmet defer={false}>
      <title>{metaData.title}</title>
      <meta name="description" content={metaData.description} />
      <meta name="Keywords" content="클라우드풀필먼트, 풀필먼트, 배송비, 하포테크, 합포" />

      {/* <!-- KaKao --> */}
      <meta property="og:url" content={metaData.url} />
      <meta property="og:title" content={metaData.title} />
      <meta property="og:type" content="website" />
      <meta property="og:image" content={metaData.image} />
      <meta property="og:description" content={metaData.description} />
      <meta property="og:site_name" content="하포테크" />
      {/* <!-- Twitter --> */}
      <meta property="twitter:card" content="card" />
      <meta property="twitter:title" content={metaData.title} />
      <meta property="twitter:image" content={metaData.image} />
      <meta property="twitter:description" content={metaData.description} />
    </Helmet>
  );
};

export default MetaTag;
