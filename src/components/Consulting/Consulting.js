import React, { useEffect, useState } from "react";
import pcLogo from "../../assets/images/Consulting/new_pc_banner.png";
import mobileLogo from "../../assets/images/Consulting/new_mobile_banner.png";
import ModalForm from "./ModalForm";
import { Link } from "react-router-dom";
import MetaTag from "../MetaTag";

function Consulting() {
  const [windowWidth, setWindowWidth] = useState(window.innerWidth);
  const [openModal, setOpenModal] = useState(false);

  useEffect(() => {
    const handleResize = () => {
      setWindowWidth(window.innerWidth);
    };

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  const isNarrowScreen = windowWidth <= 769;

  const handleCloseModal = () => {
    setOpenModal(false);
  };

  return (
    <div>
      <MetaTag
        title="배송비 할인 이벤트!"
        description="배송비를 건당 최대 500원 할인해드립니다."
        url="https://happotech.net/consulting"
        image="https://happotech.net/images/logo/img_meta_promotion.png"
      />
      {isNarrowScreen ? (
        <div style={{ position: "relative" }}>
          <img src={mobileLogo} alt="mobile_banner" style={{ width: "100%" }} />
          <div style={{ width: "100%", aspectRatio: "499 / 1127", margin: "auto", position: "absolute", top: 0 }}>
            <Link
              style={{
                position: "absolute",
                width: "32vw",
                height: "4%",
                bottom: "7.3%",
                left: "33.5%",
              }}
              onClick={() => setOpenModal(true)}
            />
          </div>
          <ModalForm open={openModal} close={handleCloseModal} />
        </div>
      ) : (
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            position: "relative",
            width: "100%",
          }}
        >
          <img src={pcLogo} style={{ width: "100%", margin: "auto" }} alt="pc_banner" />
          <div style={{ width: "100%", aspectRatio: "1920 / 1681", margin: "auto", position: "absolute" }}>
            <Link
              style={{
                position: "absolute",
                width: "16.2%",
                height: "5%",
                left: "42%",
                bottom: "8%",
              }}
              onClick={() => setOpenModal(true)}
            />
          </div>
          <ModalForm open={openModal} close={handleCloseModal} />
        </div>
      )}
    </div>
  );
  if (isNarrowScreen)
    return (
      <div style={{ position: "relative" }}>
        <img src={mobileLogo} alt="mobile_banner" style={{ width: "100%" }} />
        <div style={{ width: "100%", aspectRatio: "499 / 1127", margin: "auto", position: "absolute", top: 0 }}>
          <Link
            style={{
              position: "absolute",
              width: "32vw",
              height: "4%",
              bottom: "7.3%",
              left: "33.5%",
            }}
            onClick={() => setOpenModal(true)}
          />
        </div>
        <ModalForm open={openModal} close={handleCloseModal} />
      </div>
    );

  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        position: "relative",
        width: "100%",
      }}
    >
      {/* <Helmet>
        <title>배송비 할인 이벤트!</title>
        <meta name="description" content="배송비를 건당 최대 500원 할인해드립니다." />
        <meta name="theme-color" content="#080f2a" />

        <meta property="og:title" content="배송비 할인 이벤트!" />
        <meta property="og:type" content="website" />
        <meta property="og:image" content="%PUBLIC_URL%/images/logo/img_meta_promotion.png" />
        <meta property="og:description" content="배송비를 건당 최대 500원 할인해드립니다." />

        <meta property="twitter:card" content="card" />
        <meta property="twitter:title" content="배송비 할인 이벤트!" />
        <meta property="twitter:image" content="%PUBLIC_URL%/images/logo/img_meta_promotion.png" />
        <meta property="twitter:description" content="배송비를 건당 최대 500원 할인해드립니다." />
      </Helmet> */}
      <img src={pcLogo} style={{ width: "100%", margin: "auto" }} alt="pc_banner" />
      <div style={{ width: "100%", aspectRatio: "1920 / 1681", margin: "auto", position: "absolute" }}>
        <Link
          style={{
            position: "absolute",
            width: "16.2%",
            height: "5%",
            left: "42%",
            bottom: "8%",
          }}
          onClick={() => setOpenModal(true)}
        />
      </div>
      <ModalForm open={openModal} close={handleCloseModal} />
    </div>
  );
}

export default Consulting;
