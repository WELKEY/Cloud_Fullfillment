import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import styles from "./ModalForm.module.css";

const ModalForm = ({ open, close }) => {
  useEffect(() => {
    const initRecatch = (r, e, c, a, t, ch) => {
      var h = r.getElementsByTagName(e)[0],
        i = r.createElement(c);
      i.async = true;
      i.id = "recatch-embed-script";
      i.src =
        "https://cdn.recatch.cc/recatch-embed.iife.js?t=" +
        a[0] +
        "&b=" +
        a[1] +
        "&c=" +
        t +
        "&tr=true&th=" +
        ch +
        "&mode=sdk&pc=%2340a2d8";
      h.appendChild(i);
    };
    initRecatch(document, "head", "script", ["happotech", "eycnsotzyk"], "recatch-form", "dark");
  }, []);
  return (
    <div style={{ display: open ? "block" : "none", position: "relative" }}>
      <div className={styles.modal_overlay}></div>
      <div className={styles.modal_content}>
        <div style={{ textAlign: "right", padding: 10 }}>
          <Link onClick={close}>
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path
                d="M17.6568 6.34314L12 12M12 12L6.34314 17.6568M12 12L17.6568 17.6568M12 12L6.34314 6.34314"
                stroke="#9E9E9E"
                strokeWidth="2"
              />
            </svg>
          </Link>
        </div>
        <iframe
          src="https://happotech.recatch.cc/workflows/eycnsotzyk?theme=dark&primaryColor=%2340a2d8"
          style={{
            width: "100%",
            height: "90%",
            border: "none",
          }}
        ></iframe>
      </div>
    </div>
  );
};

export default ModalForm;
